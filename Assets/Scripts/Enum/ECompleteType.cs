﻿namespace UnityModule.Loading
{
   public enum ECompleteType
    {
        Instant = 0,
        AnyKey = 1,
    }
}