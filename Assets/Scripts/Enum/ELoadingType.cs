﻿namespace UnityModule.Loading
{
    public enum ELoadingType
    {
        Async = 0,
        Fake = 1,
    }
}