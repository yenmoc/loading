﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

namespace UnityModule.Loading
{
    public class LoadInstantComplete : ILoadComplete
    {
        public void OnFinish(ILoading iLoad)
        {
            iLoad.SetActiveTip(false);
            iLoad.DisposableTips?.Dispose();
            iLoad.DisposableWaitTips?.Dispose();
            iLoad.FadeOutProcessBar();
            iLoad.LoadNextScene();
        }

        public void Setup()
        {
        }
    }
}