﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace UnityModule.Loading
{
    public interface ILoading
    {
        bool IsTip { get; }
        void SetActiveTip(bool state);
        bool IsProcessBar { get; }
        TextMeshProUGUI TipText { get; }
        CanvasGroup CanvasGroupProcessBar { get; }
        float TimeFadeProcessBar { get; }
        ILoadComplete LoadComplete { get; }
        void LoadNextScene();
        IEnumerator FadeOutCanvas(CanvasGroup alpha, float delay = 0);
        void FadeOutProcessBar();
        IDisposable DisposableTips { get; }
        IDisposable DisposableWaitTips { get; }
        IDisposable DisposableNextScene { get; }
    }
}